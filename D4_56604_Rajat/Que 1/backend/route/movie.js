const { response } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utills')

const router = express.Router()

router.get('/',(request,response)=>{
    const query = `select * from Movie`
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.post('/add',(request,response)=>{
    const{movie_title, movie_release_date, movie_time, director_name} = request.body
    const query = `insert into Movie (movie_title, movie_release_date, movie_time, director_name) values ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.put('/update/:id',(request,response)=>{
    const{movie_release_date, movie_time} = request.body
    const{id} = request.params
    const query = `update Movie set movie_release_date = '${movie_release_date}', movie_time = '${movie_time}' where movie_id = ${id}`
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.delete('/delete/:id',(request,response)=>{
    const {id} = request.params
    const query =  `delete from Movie where movie_id = ${id}`
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    }) 
})

module.exports = router